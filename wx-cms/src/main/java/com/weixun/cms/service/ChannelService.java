package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.cms.model.vo.Channel;
import com.weixun.model.CmsChannel;
import com.weixun.model.SysMenu;

import java.util.List;

public class ChannelService {
    /**
     * 根据父菜单id获取子菜单
     * @param channel
     * @return
     */
    public List<CmsChannel> findChildList(Channel channel)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_channel where 1=1 ");
        if (channel.getId() != null)
        {
            stringBuffer.append("and channel_parent = "+channel.getId());
        }
        List<CmsChannel> list = CmsChannel.dao.find(stringBuffer.toString());
        return list;
    }


    /**
     * 根据id获取信息
     * @param cmsChannel
     * @return
     */
    public List<CmsChannel> findList(CmsChannel cmsChannel)
    {
//        SysMenu sysMenu = SysMenu.dao.findById(id);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_channel where 1=1 ");
        if (cmsChannel.getChannelPk() != null && !cmsChannel.getChannelPk().equals(""))
        {
            stringBuffer.append("and channel_pk = "+cmsChannel.getChannelPk());
        }

        List<CmsChannel> list = CmsChannel.dao.find(stringBuffer.toString());
        return list;
    }


    public CmsChannel findId(String channel_pk)
    {
//        StringBuffer stringBuffer = new StringBuffer();
//        stringBuffer.append("select * from cms_channel where 1=1 ");
//        if (channel_pk != null && !channel_pk.equals(""))
//        {
//            stringBuffer.append("and channel_pk = "+channel_pk);
//        }
        CmsChannel cmsChannel = CmsChannel.dao.findById(channel_pk);
        return cmsChannel;
    }

    public Record findById(String channel_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_channel where 1=1 ");
        if (channel_pk != null && !channel_pk.equals(""))
        {
            stringBuffer.append("and channel_pk = "+channel_pk);
        }
        Record record = Db.use("datasource").findFirst(stringBuffer.toString());
        return record;
    }
    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from cms_channel where channel_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }
}
